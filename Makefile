SH := /bin/sh

TCLSH ?= tclsh8.7
DESTDIR ?=
PREFIX ?= /usr/local

BUILD_DATE != date -R
APPVERSION != cat VERSION
RELEASE_NAME := tksysrc-$(APPVERSION)

MKDIR := mkdir -vp -m 0755
RM := rm -fv
RMDIR := rm -rfv
INSTALL := install -v -C -h md5
INSTALL_EXE := $(INSTALL) -m 0755
INSTALL_FILE := $(INSTALL) -m 0644

BUILDDIR := build/$(RELEASE_NAME)
GENDIR := build/gen/$(RELEASE_NAME)

LIB_SRCS != ls lib/tksysrc/*.tcl | grep -v pkgIndex | grep -vF version.tcl
LIB_FILES != for fn in $(LIB_SRCS); do echo $(BUILDDIR)/$$fn; done

LIBEXEC_SRCS != ls libexec/tksysrc/*
LIBEXEC_FILES != for fn in $(LIBEXEC_SRCS); do echo $(BUILDDIR)/$$fn; done

SHARE_DOCSRCS := LICENSE README.md VERSION
SHARE_DOCFILES != for fn in $(SHARE_DOCSRCS); do echo $(BUILDDIR)/share/doc/tksysrc/$$fn; done

BUILD_DEPS := $(BUILDDIR)/bin/tksysrc
BUILD_DEPS += $(BUILDDIR)/lib/tksysrc/pkgIndex.tcl
BUILD_DEPS += $(BUILDDIR)/lib/tksysrc/version.tcl
BUILD_DEPS += $(LIB_FILES)
BUILD_DEPS += $(LIBEXEC_FILES)
BUILD_DEPS += $(SHARE_DOCFILES)

DIST_DEPS := $(BUILD_DEPS)
DIST_DEPS += $(BUILDDIR)/share/doc/tksysrc/ChangeLog


.PHONY: all
all: build


.PHONY: build
build: $(BUILD_DEPS)


.PHONY: dist
dist: dist/$(RELEASE_NAME).txz


.PHONY: install
install: $(DIST_DEPS)
	@$(MKDIR) $(DESTDIR)$(PREFIX)/bin
	@$(INSTALL_EXE) $(BUILDDIR)/bin/tksysrc $(DESTDIR)$(PREFIX)/bin
	@$(MKDIR) $(DESTDIR)$(PREFIX)/lib/tksysrc
	@$(INSTALL_FILE) $(BUILDDIR)/lib/tksysrc/*.tcl \
				$(DESTDIR)$(PREFIX)/lib/tksysrc
	@$(MKDIR) $(DESTDIR)$(PREFIX)/libexec/tksysrc
	@$(INSTALL_EXE) $(BUILDDIR)/libexec/tksysrc/* \
				$(DESTDIR)$(PREFIX)/libexec/tksysrc
	@$(MKDIR) $(DESTDIR)$(PREFIX)/share/doc/tksysrc
	@$(INSTALL_FILE) $(BUILDDIR)/share/doc/tksysrc/* \
				$(DESTDIR)$(PREFIX)/share/doc/tksysrc


.PHONY: uninstall
uninstall:
	@$(RM) $(DESTDIR)$(PREFIX)/bin/tksysrc
	@$(RMDIR) $(DESTDIR)$(PREFIX)/lib/tksysrc
	@$(RMDIR) $(DESTDIR)$(PREFIX)/libexec/tksysrc
	@$(RMDIR) $(DESTDIR)$(PREFIX)/share/doc/tksysrc


.PHONY: clean
clean:
	@$(RMDIR) build dist


.PHONY: pkgindex
pkgindex: lib/tksysrc/pkgIndex.tcl


dist/$(RELEASE_NAME).txz: $(DIST_DEPS)
	@$(MKDIR) dist
	@tar -cJf dist/$(RELEASE_NAME).txz -C build \
			$(RELEASE_NAME)/bin/tksysrc \
			$(RELEASE_NAME)/lib/tksysrc \
			$(RELEASE_NAME)/libexec/tksysrc \
			$(RELEASE_NAME)/share/doc/tksysrc
	touch dist/$(RELEASE_NAME).txz


lib/tksysrc/pkgIndex.tcl: $(LIB_SRCS) lib/tksysrc/version.tcl
	echo 'pkg_mkIndex lib/tksysrc *.tcl' | $(TCLSH)


$(BUILDDIR)/lib/tksysrc/pkgIndex.tcl: lib/tksysrc/pkgIndex.tcl
	@$(MKDIR) $(BUILDDIR)/lib/tksysrc
	@$(INSTALL_FILE) lib/tksysrc/pkgIndex.tcl \
				$(BUILDDIR)/lib/tksysrc/pkgIndex.tcl


$(GENDIR)/bin/tksysrc: bin/tksysrc
	@$(MKDIR) $(GENDIR)/bin
	echo '#!/usr/bin/env $(TCLSH)' >$(GENDIR)/bin/tksysrc
	tail -n +2 bin/tksysrc >>$(GENDIR)/bin/tksysrc


$(BUILDDIR)/bin/tksysrc: $(GENDIR)/bin/tksysrc
	@$(MKDIR) $(BUILDDIR)/bin
	@$(INSTALL_EXE) $(GENDIR)/bin/tksysrc $(BUILDDIR)/bin/tksysrc


$(GENDIR)/lib/tksysrc/version.tcl: lib/tksysrc/version.tcl VERSION
	@$(MKDIR) $(GENDIR)/lib/tksysrc
	@cat lib/tksysrc/version.tcl | \
		sed 's/BUILD_DATE/BUILD_DATE "$(BUILD_DATE)"/' | \
		sed 's/VERSION master/VERSION "$(APPVERSION)"/' \
		>$(GENDIR)/lib/tksysrc/version.tcl
	touch $(GENDIR)/lib/tksysrc/version.tcl


$(BUILDDIR)/lib/tksysrc/version.tcl: $(GENDIR)/lib/tksysrc/version.tcl
	@$(MKDIR) $(BUILDDIR)/lib/tksysrc
	@$(INSTALL_FILE) $(GENDIR)/lib/tksysrc/version.tcl \
				$(BUILDDIR)/lib/tksysrc/version.tcl


$(LIB_FILES): $(LIB_SRCS)
	@$(MKDIR) $(BUILDDIR)/lib/tksysrc
	@for fn in $(LIB_SRCS); do \
		$(INSTALL_FILE) $$fn $(BUILDDIR)/$$fn; \
	done


$(GENDIR)/libexec/tksysrc/sudo-askpass: libexec/tksysrc/sudo-askpass
	@$(MKDIR) $(GENDIR)/libexec/tksysrc
	echo '#!/usr/bin/env $(TCLSH)' >$(GENDIR)/libexec/tksysrc/sudo-askpass
	tail -n +2 libexec/tksysrc/sudo-askpass >>$(GENDIR)/libexec/tksysrc/sudo-askpass


$(BUILDDIR)/libexec/tksysrc/sudo-askpass: $(GENDIR)/libexec/tksysrc/sudo-askpass
	@$(MKDIR) $(BUILDDIR)/libexec/tksysrc
	@$(INSTALL_EXE) $(GENDIR)/libexec/tksysrc/sudo-askpass \
			$(BUILDDIR)/libexec/tksysrc


$(BUILDDIR)/share/doc/tksysrc/ChangeLog: $(GENDIR)/share/doc/tksysrc/ChangeLog
	@$(MKDIR) $(BUILDDIR)/share/doc/tksysrc
	@$(INSTALL_FILE) $(GENDIR)/share/doc/tksysrc/ChangeLog \
				$(BUILDDIR)/share/doc/tksysrc


$(GENDIR)/share/doc/tksysrc/ChangeLog:
	@$(MKDIR) $(GENDIR)/share/doc/tksysrc
	which git >/dev/null && git log >$(GENDIR)/share/doc/tksysrc/ChangeLog


$(SHARE_DOCFILES): $(SHARE_DOCSRCS)
	@$(MKDIR) $(BUILDDIR)/share/doc/tksysrc
	@for fn in $(SHARE_DOCSRCS); do \
		$(INSTALL_FILE) $$fn $(BUILDDIR)/share/doc/tksysrc; \
	done
