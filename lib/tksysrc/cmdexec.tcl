# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

package provide tksysrc::cmdexec 0.0

namespace eval ::tksysrc::cmdexec {
    namespace export version list_setvars save remove
    namespace ensemble create

    variable debug 0
    variable lasterr 0
    variable errmsg ""
    variable filename ""

    # run args
    variable with_sudo 0
    variable sudo_askpass 0
    variable check_error 0
}

#
# parse args for cmdexec run
#   args for internal usage:
#       --sudo        - enable sudo
#       --check-error - report error if command failed
#       --askpass     - enable sudo ask pass
#   anything else is passed to sysrc command
#
proc ::tksysrc::cmdexec::args_parse {orig_args} {
    set l {}
    foreach {a} $orig_args {
        if {$a == "--sudo"} {
            set tksysrc::cmdexec::with_sudo 1
        } elseif {$a == "--askpass"} {
            set tksysrc::cmdexec::sudo_askpass 1
        } elseif {$a == "--check-error"} {
            set tksysrc::cmdexec::check_error 1
        } else {
            lappend l $a
        }
    }
    return $l
}

#
# run child process
#
proc ::tksysrc::cmdexec::run {desc args} {
    if {$tksysrc::cmdexec::debug} {
        puts stderr "tksysrc cmdexec: $desc $args"
    }
    set report_progress 0
    set use_pgb 0

    if {[winfo exists .view]} {
        if {$tksysrc::cmdexec::debug} {
            puts stderr "tksysrc run report progress"
        }
        set report_progress 1
        tk busy hold .view
        tk busy configure .view -cursor watch
        if {$tksysrc::cmdexec::debug} {
            puts stderr "tksysrc busy hold"
        }
        .statusbar.label configure -text [format "%s..." $desc]
        if {[winfo exists .statusbar.progress]} {
            set use_pgb 1
            .statusbar.progress configure -value 0
        } else {
            .statusbar.counter configure -text ""
        }
        update
    }

    set tksysrc::cmdexec::lasterr 0
    set tksysrc::cmdexec::errmsg ""
    set tksysrc::cmdexec::with_sudo 0
    set tksysrc::cmdexec::sudo_askpass 0
    set tksysrc::cmdexec::check_error 0
    set cmdargs [tksysrc::cmdexec::args_parse $args]

    if {$tksysrc::cmdexec::with_sudo} {
        set cmd [list /usr/local/bin/sudo -b]
        if {$tksysrc::cmdexec::sudo_askpass} {
            set tksysrc::cmdexec::sudo_askpass 0
            lappend cmd -A
        } else {
            lappend cmd -n
        }
        lappend cmd /usr/sbin/sysrc
    } else {
        set cmd [list /usr/sbin/sysrc]
    }

    if {$tksysrc::cmdexec::filename != ""} {
        lappend cmd "-f"
        lappend cmd $tksysrc::cmdexec::filename
    }

    foreach {a} $cmdargs {
        lappend cmd $a
    }

    set line_no 0
    set out_lines {}

    if {$tksysrc::cmdexec::debug} {
        puts stderr "tksysrc exec: $cmd"
    }

    set p [open "|$cmd" "r"]
    while {[gets $p line] >= 0} {
        incr line_no
        lappend out_lines $line
        if {$report_progress} {
            .statusbar.counter configure -text $line_no
            if {$use_pgb} {
                .statusbar.progress step 1
            }
        }
        update
    }

    set out [join $out_lines "\n"]

    if {[catch {close $p} err]} {
        set tksysrc::cmdexec::lasterr 1
        set tksysrc::cmdexec::errmsg $err
        if {$tksysrc::cmdexec::check_error} {
            set report_error 1
            if {$tksysrc::cmdexec::with_sudo} {
                if {$tksysrc::cmdexec::sudo_askpass} {
                    set tksysrc::cmdexec::sudo_askpass 0
                } else {
                    if {$err == "sudo: a password is required"} {
                        set report_error 0
                        set tksysrc::cmdexec::sudo_askpass 1
                    }
                }
            }
            if {$report_error} {
                puts stderr "tksysrc child error: ($desc) $err"
                tksysrc::widget showerror $desc $err
            }
        }
    }

    if {$report_progress} {
        .statusbar.label configure -text [format "%s:" $desc]
        if {$tksysrc::cmdexec::lasterr} {
            .statusbar.counter configure -text [mc "error"]
        } else {
            .statusbar.counter configure -text [mc "done"]
        }
        if {$use_pgb} {
            .statusbar.progress configure -value 0
        }
        tk busy forget .view
        if {$tksysrc::cmdexec::debug} {
            puts stderr "tksysrc busy forget"
        }
        update
    }

    if {$tksysrc::cmdexec::sudo_askpass} {
        if {$tksysrc::cmdexec::debug} {
            puts stderr "tksysrc sudo ask password"
        }
        tksysrc::cmdexec::run $desc --askpass {*}$args
    }

    # TODO: clear sudo credentials (sudo -k)?
    return $out
}

#
# return sysrc version
#
proc ::tksysrc::cmdexec::version {} {
    return [lindex [tksysrc::cmdexec::run "" --version] 0]
}

#
# return sysrc list of set variables
#
proc ::tksysrc::cmdexec::list_setvars {} {
    return [tksysrc::cmdexec::run [mc "set vars"] -a]
}

#
# save sysrc option value
#
proc ::tksysrc::cmdexec::save {opt val} {
    set reset_fn 0
    if {$tksysrc::cmdexec::filename == ""} {
        set reset_fn 1
        set tksysrc::cmdexec::filename "/etc/rc.conf.local"
    }

    set rc [tksysrc::cmdexec::run [mc "sysrc save"] --sudo --check-error \
                                                    [format "%s=%s" $opt $val]]

    if {$reset_fn} {
        set tksysrc::cmdexec::filename ""
    }
    return $rc
}

#
# remove sysrc option
#
proc ::tksysrc::cmdexec::remove {name} {
    if {$tksysrc::cmdexec::debug} {
        puts stderr "cmdexec remove: $name"
    }
    return [tksysrc::cmdexec::run [mc "sysrc remove"] \
                                  --sudo --check-error -x $name]
}
