# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

package provide tksysrc::load 0.0

namespace eval ::tksysrc::load {
    set libexec [file join [file dirname [info script]] .. .. libexec tksysrc]
    array set ::env [list SUDO_ASKPASS [file join $libexec sudo-askpass]]
}

# load all packages

package require tksysrc::cmdexec
package require tksysrc::opt
package require tksysrc::version
package require tksysrc::view

package require tksysrc::widget
package require tksysrc::widget::cmdout
package require tksysrc::widget::menu
package require tksysrc::widget::showvars
package require tksysrc::widget::view
