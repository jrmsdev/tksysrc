# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

package provide tksysrc::opt 0.0

namespace eval ::tksysrc::opt {
    namespace export get_type save remove
    namespace ensemble create
}

#
# return the type for a sysrc option
#
proc ::tksysrc::opt::get_type {name val} {
    # TODO: check against default value?
    set nlen [string length $name]
    set lastidx [expr $nlen - 1]
    if {$nlen > 7 && [string last "_enable" $name $lastidx] >= 1} {
        return "bool"
    }
    return "str"
}

#
# save a sysrc option vale
#
proc ::tksysrc::opt::save {name val} {
    tksysrc::cmdexec save $name $val
    return $tksysrc::cmdexec::lasterr
}

#
# remove sysrc option
#
proc ::tksysrc::opt::remove {name} {
    tksysrc::cmdexec remove $name
    return $tksysrc::cmdexec::lasterr
}
