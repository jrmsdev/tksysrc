# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

package provide tksysrc::version 0.0

namespace eval ::tksysrc::version {
    namespace export string
    namespace ensemble create

    variable VERSION master
    variable BUILD_DATE
}

#
# return a string representing the full version
#
proc ::tksysrc::version::string {} {
    return [format "tksysrc version %s" $tksysrc::version::VERSION]
}
