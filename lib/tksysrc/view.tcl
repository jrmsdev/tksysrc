# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

package provide tksysrc::view 0.0

namespace eval ::tksysrc::view {
    namespace export main
    namespace ensemble create
}

#
# main view
#
proc ::tksysrc::view::main {} {
    set top [tksysrc::widget topwindow "" {300 600}]
    set w [tksysrc::widget::view create $top]

    set m [tksysrc::widget::menu create $top]
    tksysrc::widget::menu command $m [mc "_Quit"] [list destroy $top]

    set vars [tksysrc::widget::showvars create $w]
    tksysrc::widget::showvars load $vars [tksysrc::cmdexec list_setvars]

    tkwait window $top
    return 0
}
