# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

package provide tksysrc::widget::cmdout 0.0

namespace eval ::tksysrc::widget::cmdout {
    namespace export create
    namespace ensemble create
}

#
# create a widget to show commands output
#   return widget with following attributes:
#       .text - text widget
#
proc ::tksysrc::widget::cmdout::create {parent {state "normal"} {row 0} \
                                               {col 0} {sticky "nwse"}} {
    set w [tksysrc::widget mkframe $parent cmdout $row $col $sticky]

    set out [tksysrc::widget mktext $w text]
    $out configure -bg "black" -fg "green" -font "monospace 10"
    $out configure -state $state

    return $w
}
