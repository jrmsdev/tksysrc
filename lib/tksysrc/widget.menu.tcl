# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

package provide tksysrc::widget::menu 0.0

namespace eval ::tksysrc::widget::menu {
    namespace export create command
    namespace ensemble create
}

#
# create a toplevel menu
#
proc ::tksysrc::widget::menu::create {parent} {
    set m $parent.menu
    if {$parent == "."} {
        set m .menu
    }
    menu $m
    $parent configure -menu $m
    return $m
}

#
# format menu command label
#   return the underline id and the formated label ("_" char removed)
#
proc ::tksysrc::widget::menu::label_format {lbl} {
    set u [string first "_" $lbl]
    set n [string replace $lbl $u $u ""]
    return [list $u $n]
}

#
# add a menu command
#
proc ::tksysrc::widget::menu::command {m lbl func} {
    set l [tksysrc::widget::menu::label_format $lbl]
    $m add command -label [lindex $l 1] -underline [lindex $l 0] -command $func
}
