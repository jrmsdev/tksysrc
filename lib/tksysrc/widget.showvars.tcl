# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

package provide tksysrc::widget::showvars 0.0

namespace eval ::tksysrc::widget::showvars {
    namespace export create load render
    namespace ensemble create

    variable debug 0
    variable opt
    variable optidx
    variable optdb
    variable opt_widget ""
    variable render_widget ""
}

#
# create a widget to show variables list
#
proc ::tksysrc::widget::showvars::create {parent {row 0} {col 0} {s "nwse"}} {
    set w [tksysrc::widget mkframe $parent showvars $row $col $s]

    tksysrc::widget rowcfg $w 0 1
    tksysrc::widget rowcfg $w 1 19
    tksysrc::widget colcfg $w 0 1

    set tksysrc::widget::showvars::opt_widget $w.option
    tksysrc::widget mkframe $w option 0 0
    tksysrc::widget rowcfg $w.option 0 1
    tksysrc::widget colcfg $w.option 0 0
    tksysrc::widget colcfg $w.option 1 1

    tksysrc::widget mklabel $w.option label "" 0 0 w
    $w.option configure -takefocus 0
    tksysrc::widget mkframe $w.option value 0 1

    tksysrc::widget mkframe $w f 1 0
    tksysrc::widget rowcfg $w.f 0 1
    tksysrc::widget colcfg $w.f 0 0
    tksysrc::widget colcfg $w.f 1 1

    listbox $w.f.vars -selectmode "browse" -setgrid 0 \
                      -yscrollcommand [list $w.f.sb set]
    grid $w.f.vars -row 0 -column 1 -sticky nwse

    tksysrc::widget colcfg $w.f.vars 0 1

    if {$tksysrc::widget::use_ttk} {
        ttk::scrollbar $w.f.sb
    } else {
        scrollbar $w.f.sb
    }
    $w.f.sb configure -command [list $w.f.vars yview] -takefocus 0
    grid $w.f.sb -row 0 -column 0 -sticky nwse

    bind $w.f.vars <<ListboxSelect>> \
                   [list tksysrc::widget::showvars::list_selected $w.f.vars]

    bind $w.f.vars <Delete> [list tksysrc::widget::showvars::delete $w.f.vars]

    return $w.f
}

#
# load variables from sysrc command output
#
proc ::tksysrc::widget::showvars::load {w cmdout} {
    foreach {line} [split $cmdout "\n"] {
        set opt [string trim [lindex [split $line ":"] 0]]
        set val [string trim [lindex [split $line ":"] 1]]
        dict set tksysrc::widget::showvars::opt $opt $val
    }
    set tksysrc::widget::showvars::render_widget $w
    tksysrc::widget::showvars::render
}

#
# render loaded variables
#
proc ::tksysrc::widget::showvars::render {{reload "none"} {select_idx 0}} {
    set w $tksysrc::widget::showvars::render_widget
    if {$reload == "reload"} {
        if {[dict size $tksysrc::widget::showvars::optdb] > 0} {
            $w.vars delete 0 end
        }
    }
    if {[info exists tksysrc::widget::showvars::opt]} {
        set idx 0
        foreach {opt} [dict keys $tksysrc::widget::showvars::opt] {
            dict set tksysrc::widget::showvars::optdb $idx $opt
            dict set tksysrc::widget::showvars::optidx $opt $idx
            $w.vars insert $idx $opt
            incr idx
        }
        if {[dict size $tksysrc::widget::showvars::optdb] > $select_idx} {
            $w.vars selection set $select_idx
            $w.vars activate $select_idx
            $w.vars see $select_idx
            tksysrc::widget::showvars::list_selected $w.vars $select_idx
        }
    }
    focus $w.vars
}

#
# manage a list selection
#
proc ::tksysrc::widget::showvars::list_selected {vars {idx -1}} {
    if {$idx < 0} {
        set idx [$vars curselection]
        if {$idx == ""} {
            return
        }
    }
    tksysrc::widget::showvars::show_option $idx
}

#
# properly show an option and its value
#
proc ::tksysrc::widget::showvars::show_option {idx} {
    if {! [info exists tksysrc::widget::showvars::optdb]} {
        return
    }
    set opt [dict get $tksysrc::widget::showvars::optdb $idx]
    set option $tksysrc::widget::showvars::opt_widget
    $option.label configure -text [format "%s:" $opt]
    tksysrc::widget::showvars::show_value $option $opt
}

#
# properly show an option value
#
proc ::tksysrc::widget::showvars::show_value {parent opt} {
    destroy $parent.value
    set w [tksysrc::widget mkframe $parent value 0 1 we]
    tksysrc::widget rowcfg $w 0 1
    tksysrc::widget colcfg $w 0 0
    tksysrc::widget colcfg $w 1 1

    set val [dict get $tksysrc::widget::showvars::opt $opt]

    set opt_type [tksysrc::opt get_type $opt $val]
    if {$opt_type == "bool" } {
        tksysrc::widget::showvars::show_bool $w $opt $val
    } elseif {$opt_type == "str" } {
        tksysrc::widget::showvars::show_str $w $opt $val
    } else {
        tksysrc::widget mklabel $w val $val 0 1 w
        $w.val configure -foreground "red"
    }
}

#
# manage saving option value
#
proc ::tksysrc::widget::showvars::opt_save {opt val} {
    set err [tksysrc::opt save $opt $val]
    if {! $err} {
        set optidx [dict get $tksysrc::widget::showvars::optidx $opt]
        dict set tksysrc::widget::showvars::opt $opt $val
        tksysrc::widget::showvars render "reload" $optidx
    }
}

#
# properly show a bool value
#
proc ::tksysrc::widget::showvars::show_bool {parent opt val} {
    set m [menu $parent.menu -tearoff 0]
    $m add command -label "YES" \
                -command [list tksysrc::widget::showvars::opt_save $opt "YES"]
    $m add command -label "NO" \
                -command [list tksysrc::widget::showvars::opt_save $opt "NO"]

    tksysrc::widget mklabel $parent val $val 0 1 w
    $parent.val configure -relief "raised"

    bind $parent.val <1> [list tk_popup $m %X %Y]
}

#
# properly show an str value
#
proc ::tksysrc::widget::showvars::show_str {parent opt val} {
    set w [tksysrc::widget mkentry $parent val 0 1 we]
    $w insert end $val
    bind $w <Return> [list tksysrc::widget::showvars::save_str $w $opt]
}

#
# save an str option value
#
proc ::tksysrc::widget::showvars::save_str {w opt} {
    set newval [$w get]
    tksysrc::widget::showvars::opt_save $opt $newval
}

#
# manage option delete
#
proc ::tksysrc::widget::showvars::delete {vars} {
    set idx [$vars curselection]
    if {$idx == ""} {
        if {$tksysrc::widget::showvars::debug} {
            puts stderr "ignore delete empty selection"
        }
        return
    }
    set opt [dict get $tksysrc::widget::showvars::optdb $idx]
    set try_rm [tk_messageBox -icon question -type yesno \
                -message [mc "Remove '%s' setting?" $opt]]
    if {$tksysrc::widget::showvars::debug} {
        puts stderr "widget showvars delete: $opt $try_rm"
    }
    if {$try_rm == "no"} {
        return
    }
    set err [tksysrc::opt::remove $opt]
    if {! $err} {
        dict unset tksysrc::widget::showvars::opt $opt
        tksysrc::widget::showvars render "reload" 0
    }
}
