# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

package provide tksysrc::widget 0.0

namespace eval ::tksysrc::widget {
    namespace export topwindow showerror
    namespace export menu_create menu_command
    namespace export rowcfg colcfg padding
    namespace export mkframe mktext mklabel mkentry
    namespace ensemble create

    variable debug 0
    variable use_ttk 0
}

#
# create a toplevel window
#
proc ::tksysrc::widget::topwindow {{name ""} {wsize {400 300}} {title ""}} {
    set top .
    if {$name != ""} {
        set top .$name
    }
    if {$title == ""} {
        set title "tksysrc"
    }
    wm title $top $title
    wm resizable $top 1 1
    wm minsize $top {*}$wsize
    tksysrc::widget rowcfg $top 0 1
    tksysrc::widget colcfg $top 0 1
    return $top
}

#
# show error message as a toplevel child window
#
proc ::tksysrc::widget::showerror {msg info} {
    tk_messageBox -title "tksysrc error" -type ok -icon error \
                  -message $msg -detail $info
}

#
# widget grid row configure
#
proc ::tksysrc::widget::rowcfg {w {row 0} {rweight 1}} {
    grid rowconfigure $w $row -weight $rweight
    if {$tksysrc::widget::debug} {
        puts stderr "widget rowcfg: $w $row $rweight"
    }
}

#
# widget grid column configure
#
proc ::tksysrc::widget::colcfg {w {col 0} {cweight 1}} {
    grid columnconfigure $w $col -weight $cweight
    if {$tksysrc::widget::debug} {
        puts stderr "widget colcfg: $w $col $cweight"
    }
}

#
# set widget padding
#
proc ::tksysrc::widget::padding {w padx {pady -1}} {
    if {$pady < 0} {
        set pady $padx
    }
    if {$tksysrc::widget::use_ttk} {
        $w configure -padding [list $padx $pady]
    } else {
        $w configure -padx $padx -pady $pady
    }
}

#
# create a frame widget
#
proc ::tksysrc::widget::mkframe {parent name {row 0} {col 0} {sticky "nwse"}} {
    set w $parent.$name
    if {$parent == "."} {
        set w .$name
    }
    if {$tksysrc::widget::use_ttk} {
        ttk::frame $w
    } else {
        frame $w
    }
    grid $w -row $row -column $col -sticky $sticky
    if {$tksysrc::widget::debug} {
        puts stderr "widget mkframe: $w grid $row $col $sticky"
    }
    tksysrc::widget rowcfg $w 0 1
    tksysrc::widget colcfg $w 0 1
    return $w
}

#
# create a text widget
#
proc ::tksysrc::widget::mktext {parent name {row 0} {col 0} {sticky "nwse"}} {
    set w $parent.$name
    text $w
    grid $w -row $row -column $col -sticky $sticky
    if {$tksysrc::widget::debug} {
        puts stderr "widget mktext: $w grid $row $col $sticky"
    }
    return $w
}

#
# create a label widget
#
proc ::tksysrc::widget::mklabel {parent name text {row 0} {col 0}
                                                  {sticky "nwse"}} {
    set w $parent.$name
    if {$tksysrc::widget::use_ttk} {
        ttk::label $w
    } else {
        label $w
    }
    $w configure -text $text
    grid $w -row $row -column $col -sticky $sticky
    if {$tksysrc::widget::debug} {
        puts stderr "widget mktext: $w grid $row $col $sticky"
    }
    return $w
}

#
# create an entry widget
#
proc ::tksysrc::widget::mkentry {parent name {row 0} {col 0} {sticky "nwse"}} {
    set w $parent.$name
    if {$tksysrc::widget::use_ttk} {
        ttk::entry $w
    } else {
        entry $w
    }
    grid $w -row $row -column $col -sticky $sticky
    if {$tksysrc::widget::debug} {
        puts stderr "widget mkentry: $w grid $row $col $sticky"
    }
    return $w
}
