# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

package provide tksysrc::widget::view 0.0

namespace eval ::tksysrc::widget::view {
    namespace export create
    namespace ensemble create
}

#
# create a main view widget
#
proc ::tksysrc::widget::view::create {parent} {
    set w [tksysrc::widget mkframe $parent view]
    if {$parent == "."} {
        # attach status bar to main view
        tksysrc::widget rowcfg . 0 1
        tksysrc::widget rowcfg . 1 0

        tksysrc::widget mkframe . statusbar 1 0
        .statusbar configure -takefocus 0

        tksysrc::widget rowcfg .statusbar 0 1

        tksysrc::widget colcfg .statusbar 0 0
        tksysrc::widget colcfg .statusbar 1 1

        tksysrc::widget mklabel .statusbar label "started" 0 0 w
        .statusbar.label configure -takefocus 0

        tksysrc::widget mklabel .statusbar counter "" 0 1 w
        .statusbar.counter configure -takefocus 0

        if {$tksysrc::widget::use_ttk} {
            tksysrc::widget colcfg .statusbar 2 0
            ttk::progressbar .statusbar.progress -mode "determinate"
            grid .statusbar.progress -row 0 -column 2 -sticky e
        }
    }
    tksysrc::widget padding $w 1 1
    return $w
}
