#!/bin/sh -eu
TCLLIBPATH=`realpath ./lib`
TKSYSRC=`realpath ./bin/tksysrc`
export TCLLIBPATH
exec ${TKSYSRC} $@
